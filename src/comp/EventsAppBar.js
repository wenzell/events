import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import { Link } from 'react-router-dom';
import { withTheme, withStyles } from '@material-ui/core/styles';

const styles = {
    root: {
        flexGrow: 1,
    },
    flex: {
        flex: 1,
        '@media screen and (max-width:768px)':{

        }
    },
    toolbar :{
        paddingLeft:'16px',
        paddingRight:'16px',
        minHeight:'64px'
    },
    formcontrol:{
        width:'100%'
    },
    searchbar:{
        backgroundColor:'rgba(255,255,255,0.2)',
        padding:'5px',
        paddingLeft: '10px',
        borderRadius:'5px',
        marginLeft:'auto',
        right:0,
        color:'#fff',
        '&:hover':{
            backgroundColor:'rgba(255,255,255,0.3)',
        }
    },
    input:{
        width:'100px',
        transition:'0.5s ease-in-out',
        right:0,
        '&:focus':{
            width:'300px',
        },
        '@media screen and (max-width:768px)' :{
            width:'25px',
            '&:focus':{
                width:'200px'
            }
        },
    }
};


class EventsAppBar extends React.Component{
    constructor(props){
        super(props);

        this.classes = this.props.classes;
        this.state = {
            searchActive: false
        }
        this.handleSearchFocus = this.handleSearchFocus.bind(this);
    }


    handleSearchFocus = (f) =>{
        this.setState({
            searchActive: f
        })
    }

    render(){
        return(
            <div className={this.classes.root}>
                <AppBar elevation={0} style={{minHeight:"64px",zIndex:1300}}>
                    <Toolbar className={this.classes.toolbar}>
                        <IconButton onClick={()=>{this.props.handleDrawer()}} color="inherit" aria-label="Menu" style={{marginRight:'5px'}}>
                            <MenuIcon />
                        </IconButton>
                        <Link to="/" style={{ textDecoration: 'none',color:'inherit' }}><Typography classes={{root:this.classes.flex}} color="inherit" variant="title">Events</Typography></Link>
                        <FormControl classes={{root:this.classes.formcontrol}}>
                            <Input onBlur={()=>{this.handleSearchFocus(false)}} onFocus={()=>{this.handleSearchFocus(true)}} disableUnderline id="search-bar" classes={{root:this.classes.searchbar, input:this.classes.input}} startAdornment={
                                <InputAdornment position="start">
                                    <SearchIcon/>
                                </InputAdornment>
                            } />
                        </FormControl>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}

export default withTheme()(withStyles(styles)(EventsAppBar));