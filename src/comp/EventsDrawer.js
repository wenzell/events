import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import Typography from '@material-ui/core/Typography';
import classnames from 'classnames';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider'
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


import Dropper from './subcomp/Dropper';

import { withTheme, withStyles } from '@material-ui/core/styles';


const districts = [
    {value: 1, label: 'Northwest California'},
    {value: 2, label: 'Pacific Northwest'},
    {value: 3, label: 'Antarctica'},
    {value: 4, label: 'Western Canada'},
    {value: 5, label: 'Southwest California'},
    {value: 6, label: 'Northeastern Seaboard'},
    {value: 7, label: 'Reddit'},
    {value: 8, label: 'More Canada'},
];

const locales = [
    {value: 1, label: 'Concord'},
    {value: 2, label: 'Daly City'},
    {value: 3, label: 'Seven'},
    {value: 4, label: 'Canadia'},
];

const vicinities = [
    {value: 1, label: '0 to 5'},
    {value: 2, label: '6 to 15'},
    {value: 3, label: '16 to 25'},
    {value: 4, label: '26 to 50'},
]

const styles= {
    drawer: {
        width:'300px',
        paddingTop:'64px',
        transition: '0.3s ease-in-out !important',
        overflow:'hidden',
        '@media screen and (max-width:768px)': {
            width:'100vw',
            height:'100vh',
        }
    },
    center: {
        alignSelf: 'center'
    },
    dcontainer:{
        display:'flex',
        padding:'20px',
        paddingBottom:'100px',
        flexFlow: 'column',
        overflow: 'auto !important',
        '& > *' :{
            flexShrink:0
        }
    },
    collapse: {
        overflow:'auto'
    },
    container : {
        display:'flex',
        alignItems:'flex-end'
    },
    findevent: {
        marginBottom: '10px',
        fontSize: '1.75em'
    },
    hr : {
        margin: '15px -20px'
    },
    vicinityClass : {
        width: '40%'
    },
    ofText : {
        flex: '1',
        margin: '1px 10px'
    },
    zipForm : {
        marginBottom: '5px',
        width: '40%'
    },
    checkTitle : {
        marginTop: '10px'
    },
    checkSet : {
        margin: '0 15px'
    },
    dropDownIcon :{
        alignSelf:'center',
        transform:'rotate(0)',
        transition:'0.25s',
        '&.collapsed':{
            transform: 'rotateX(-180deg)'
        }
    },
    finddropper : {
        alignSelf: 'center',
        letterSpacing:'0.3em',
        marginTop:'10px'
    }
};

class EventsDrawer extends React.Component{
    constructor(props){
        super(props);
        this.classes = this.props.classes;

        this.state= {
            drawer : true,
            drawerCollapsed: true,
            selectedDistrict : 0,
            selectedLocale: 0,
            selectedVicinity: 0,
            locationCheck : false,
            eventTypeChecked:[
                {label:'Food', value:false, id: 1},
                {label:'Sports', value:false, id: 2},
                {label:'Music', value:false, id: 3},
                {label:'Arts', value:false, id: 4},
                {label:'Family', value:false, id: 5},
            ],
            eventStatusChecked: [
                {label:'Passed', value:false, id: 1},
                {label:'Happening Now', value:false, id: 2},
                {label:'Upcoming', value:false, id: 3},
            ]
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleDropDown = this.handleDropDown.bind(this);
    }

    handleChange = (valName, val) => {
        this.setState({
            [valName] : val
        })
    }

    handleDropDown = () =>{
        this.setState({
            drawerCollapsed: !this.state.drawerCollapsed
        })
    }


    render(){
        return(
            <Drawer classes={{paper:this.classes.drawer}} variant="persistent" open={this.props.drawerOpen}>
                <List>
                    <ListItem button>
                        <ListItemText primary="My Events" />
                    </ListItem>
                    <ListItem button>
                        <ListItemText secondary="Add/Edit" />
                    </ListItem>
                    <ListItem button>
                        <ListItemText secondary="Registration" />
                    </ListItem>
                    <ListItem button>
                        <ListItemText secondary="Reporting" />
                    </ListItem>
                </List>
                
                <Collapse className={this.classes.collapse} in={!this.state.drawerCollapsed}>
                <Divider/>
                <div className={this.classes.dcontainer}>
                    <Typography className={this.classes.findevent} variant="subheading" >Find Event</Typography>
                    <Dropper label="District" doChange={this.handleChange} valName="selectedDistrict" value={this.state.selectedDistrict} list={districts} />
                    <Dropper label="Locale" disabled={this.state.selectedDistrict === 0} doChange={this.handleChange}  valName="selectedLocale" value={this.state.selectedLocale} list={locales} />
                    <Typography style={{alignSelf:'center',margin:'10px'}} variant="caption">or</Typography>
                    <div className={this.classes.container}>
                        <Dropper label="Within" className={this.classes.vicinityClass} doChange={this.handleChange}  valName="selectedVicinity" value={this.state.selectedVicinity} list={vicinities} inputProps="mi" />
                        <Typography className={this.classes.ofText} align="center" variant="caption">of</Typography>
                        <TextField label="zip code" className={this.classes.zipForm} inputProps={{maxLength : '5'}}  />
                    </div>
                    <FormGroup className={this.classes.center} row>
                        <FormControlLabel control={
                            <Checkbox checked={this.state.locationCheck} onChange={()=>{this.setState({locationCheck:!this.state.locationCheck})}} value="Use my location" />
                        } label="Use my location" />
                    </FormGroup>
                    <Divider className={this.classes.hr} />
                    <Typography className={this.classes.checkTitle} variant="body2">Event Type</Typography>
                    <FormControl className={this.classes.checkSet} component="fieldset">
                        <FormGroup>
                            {this.state.eventTypeChecked.map(o=>{return (
                                <FormControlLabel key={o.id} control={
                                    <Checkbox/>
                                }
                                label={o.label} />
                            )})}
                        </FormGroup>
                    </FormControl>
                    <Divider className={this.classes.hr} />
                    <Typography className={this.classes.checkTitle} variant="body2">Status</Typography>
                    <FormControl className={this.classes.checkSet} component="fieldset">
                        <FormGroup>
                            {this.state.eventStatusChecked.map(o=>{return (
                                <FormControlLabel key={o.id} control={
                                    <Checkbox/>
                                }
                                label={o.label} />
                            )})}
                        </FormGroup>
                    </FormControl>
                </div>
                </Collapse>
                <Divider light />
                <Typography className={this.classes.finddropper} variant="caption">Find Events</Typography>
                <IconButton onClick={()=>{this.handleDropDown()}} className={classnames(this.classes.dropDownIcon,(this.state.drawerCollapsed ? '' : 'collapsed' ))}>
                    <ExpandMoreIcon/>
                </IconButton>
                
            </Drawer>
        )
    }
}

export default withTheme()(withStyles(styles)(EventsDrawer));