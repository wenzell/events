import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment'
import TextField from '@material-ui/core/TextField';
import { withTheme, withStyles } from '@material-ui/core/styles';

const styles= {
    textfield:{
        marginBottom:'5px',
    }
};

class Dropper extends React.Component{
    constructor(props){
        super(props);
        this.classes = this.props.classes;
        this.state= {
            
        }
    }

    handleChange = prop => event => {
        this.props.doChange(prop,event)
    }

    render(){
        return(
            <TextField disabled={this.props.disabled} style={this.props.style} select classes={{root:this.classes.textfield}} onChange={(event)=>{this.props.doChange(this.props.valName, event.target.value)}} label={this.props.label} value={this.props.value} InputProps={{startAdornment: (this.props.inputProps ? <InputAdornment position="start">{this.props.inputProps}</InputAdornment> : "")}}>
                {this.props.list.map(option=>(
                    <MenuItem key={option.value} value={option.value}>{option.label}</MenuItem>
                ))}
            </TextField>
        )
    }
}

export default withTheme()(withStyles(styles)(Dropper));