import React from 'react';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Divider from '@material-ui/core/Divider';
import Collapse from '@material-ui/core/Collapse';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Grow from '@material-ui/core/Zoom';
import Chip from '@material-ui/core/Chip';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import PinDropIcon from '@material-ui/icons/Place';
import AddIcon from '@material-ui/icons/Add';
import FBIcon from 'mdi-material-ui/Facebook';
import TwitterIcon from 'mdi-material-ui/Twitter';
import { Link } from 'react-router-dom';
import { withTheme, withStyles } from '@material-ui/core/styles';

const styles= {
    card:{
        width:'360px',
        position:'relative'
    },
    image: {
        height:0,
        paddingTop: '56.25%',
        filter: 'brightness(0.6)',
    },
    cardhead :{
        position: 'absolute',
        top:0,
        left:0,
        height:'202.5px',
        cursor:'pointer',
        width:'100%',
        '&:hover':{
            backgroundColor:'rgba(0,0,0,0.3)'
        },
        '&:active':{
            backgroundColor:'rgba(0,0,0,0.4)'
        }
    },
    cardheadgroup: {
        position:'absolute',
        width: '360px',
        bottom:'0',
        padding:'10px 15px',
        '& > *':{
            lineHeight:'1em',
            color:'#fff'
        }
    },
    title: {
        fontWeight:'500'
    },
    subtitle : {
        fontWeight:'300',
        fontSize:'1.2em'
    },
    cardEventType : {
        width:'100%',
        display:'flex',
    },
    type:{
        padding: '5px 10px',
        color: '#fff',
        flexGrow:'0',
        fontSize: '0.85em',
        fontWeight: '500',
        '&.food':{
            'background-color':'#EC6F86',
        },
        '&.sports':{
            'background-color':'#4BB052',
        },
        '&.music':{
            'background-color':'#3F69D4',
        },
        '&.arts':{
            'background-color':'#AD61ED'
        },
        '&.family':{
            'background-color':'#F8981D'
        },
    },
    chips : {
        height:'60px',
        alignItems:'center',
        display: 'flex',
        flexFlow:'row wrap',
        padding:'10px 24px',
    },
    chipRoot:{
        margin:'5px',
        fontSize:'0.70em',
        fontWeight:'300',
        height:'1.75em',
        cursor:'pointer',
        '&:hover':{
            backgroundColor:'#eee'
        },
        '&:active':{
            transition:'0s',
            backgroundColor:'#aaa'
        },
        '& >  span':{
            paddingLeft:'8px',
            paddingRight:'8px'
        }
    },
    chipAvatar : {
        backgroundColor: '#00bcd4',
        color: '#fff',
        height:'1.5em',
        width:'1.5em',
        fontSize:'1.1em',
        fontWeight:500,
    },
    expand: {
        marginLeft: 'auto',
        transition:'0.3s',
        transform:'rotate(0)'
    },
    expandrot:{
        transform:'rotate(180deg)'
    },
    addressSection:{
        display:'flex',
        alignItems: 'center',
        transition:'0.3s',
        padding:'10px 24px',
        cursor:'pointer',
        '&:hover':{
            '& > svg':{
                transform: 'translate3D(0,-0.35em,0)'
            }
        },
        '&>svg':{
            width:'1.5em',
            height:'1.5em',
            transition:'0.3s',
        }

    },
    descP : {
        marginTop:'10px',
        height: '60px',
        overflow:'auto',
        paddingTop:0,
        paddingBottom:0,
        '& > p':{
            color:'#949494',
        }
    },
    regSection: {
        display:'flex',
        alignItems:'center',
        justifyContent:'flex-start',
    },
    regNum:{
        padding: '5px 10px',
        minWidth: '20%',
        borderRadius: '10px',
        color: '#fff',
        fontWeight: '300',
        fontSize: '1.5em',
        marginRight: '30px',
        textAlign: 'center',
        '&.grey':{
            backgroundColor:'#7a7a7a'
        },
        '&.prim':{
            backgroundColor:'#00bcd4'
        }
    },
    addButton :{
        marginLeft: 'auto'
    }
    
};


class EventCard extends React.Component{
    constructor(props){
        super(props);
        this.classes = this.props.classes;

        this.state= {
            expanded:false,
        }
    }

    render(){
        return(
            <Card classes={{root:this.classes.card}}>
                            <Link to="/event/413" ><CardMedia className={this.classes.image} image="http://via.placeholder.com/1280x720" title="cardimg"/></Link>
                            <Link to="/event/413" ><div className={this.classes.cardhead}>
                                <div className={this.classes.cardheadgroup}>
                                    <Typography noWrap className={this.classes.title} variant="display1">Event Title</Typography>
                                    <Typography className={this.classes.subtitle} variant="subheading">Event Date</Typography>
                                </div>
                                <div className={this.classes.cardEventType}>
                                    <div className={classnames(this.classes.type,'food')}>
                                        FOOD
                                    </div>
                                </div>
                            </div>
                            </Link>
                            <CardContent className={this.classes.chips}>
                                <Chip classes={{root:this.classes.chipRoot}} avatar={<Avatar classes={{root:this.classes.chipAvatar}}>D</Avatar>} label="Northwest California" />
                                <Chip classes={{root:this.classes.chipRoot}} avatar={<Avatar classes={{root:this.classes.chipAvatar}}>L</Avatar>} label="Canadia" />
                            </CardContent>
                            <CardContent className={this.classes.addressSection}>
                                <PinDropIcon color="primary"/>
                                <div className={this.classes.address}>
                                    <Typography>2433 Street Ct.</Typography>
                                    <Typography>San Francisco, CA 94524</Typography>
                                </div>
                            </CardContent>
                            <CardContent className={this.classes.descP}>
                                <Typography component="p">
                                Join the Local Congregation of San Francisco for their bi-monthly cooking night. This time it's going to be sushi! Bring your friends and family.
                                </Typography>
                            </CardContent>
                            <Collapse in={this.state.expanded} unmountOnExit>
                                <Divider style={{margin:'20px 0'}}/>
                                <CardContent className={this.classes.regSection}>
                                    <div className={classnames(this.classes.regNum, 'grey')}>830</div>
                                    <Typography variant="subheading">Registered to attend</Typography>
                                </CardContent>
                                <CardContent className={this.classes.regSection}>
                                    <div className={classnames(this.classes.regNum, 'prim')}>0</div>
                                    <Typography variant="subheading">Your Guests</Typography>
                                    <IconButton className={this.classes.addButton}>
                                        <AddIcon />
                                    </IconButton>
                                </CardContent>
                                <Divider />
                                
                            </Collapse>
                            <CardActions>
                                    <Grow in={this.state.expanded} style={{transitionDelay: this.state.expanded ? 150 : 0 }}>
                                        <IconButton>
                                            <FBIcon style={{color:'#3B5998'}} />
                                        </IconButton>
                                    </Grow>
                                    <Grow in={this.state.expanded} style={{transitionDelay: this.state.expanded ? 300 : 0 }}>
                                        <IconButton>
                                            <TwitterIcon style={{color:'#00aced'}} />
                                        </IconButton>
                                    </Grow>
                                    <IconButton onClick={()=>{this.setState({expanded:!this.state.expanded})}} className={classnames(this.classes.expand, (this.state.expanded ? this.classes.expandrot : null))}>
                                        <ExpandMoreIcon  />
                                    </IconButton>
                                </CardActions>
                        </Card>
        )
    }
}

export default withTheme()(withStyles(styles)(EventCard));