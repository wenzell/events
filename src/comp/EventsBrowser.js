import React from 'react';
import classnames from 'classnames';
import { withTheme, withStyles } from '@material-ui/core/styles';
import { Switch, Route } from 'react-router-dom';
import EventsHome from './routes/EventsHome';
import EventPage from './routes/EventPage';

const styles= {
    eventsbrowser: {
        width:'100%',
        marginTop: '64px',
        display:'flex'
    },
    aside:{
        width:'320px',
        transition:'0.4s ease-in-out',
        '&.active':{
            width:'0',
            transition:'0.3s ease-in-out'
        }
    },
    eventcontainer : {
        flex:1,
        width:'100%',
        right:0,
        transition:'0.4s ease-in-out',
        '&.active':{
            paddingLeft:'300px',
            '@media screen and (max-width:768px)':{
                paddingLeft:'0'
            }
        }
    }
    
};

class EventsBrowser extends React.Component{
    constructor(props){
        super(props);
        this.classes = this.props.classes;

        this.state= {
            
        }
    }

    render(){
        return(
            <div className={this.classes.eventsbrowser }  >
                <div className={classnames(this.classes.eventcontainer,(this.props.drawerOpen ?'active':''))}>
                <Switch>
                    <Route exact path="/" component={EventsHome} />
                    <Route path="/event/:eventId" component={EventPage} />
                </Switch>
                </div>
            </div>
        )
    }
}

export default withTheme()(withStyles(styles)(EventsBrowser));