import React from 'react';
import { withTheme, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';



const styles= {
    rootContainer:{
        display:'flex',
        flexFlow:'column',
        alignItems:'center',
        width:'100%'
    },
    regOneRoot:{
        display:'flex',
        flexFlow:'column',
        padding:'20px',
    },
    question:{
        marginTop:'40px',
        marginBottom:'10px'
    },
    regInfo:{
        display:'flex',
        flexFlow:'column',
        marginLeft:'10px'
    },
    regName:{
        display:'flex'
    },
    nameSec:{
        display:'flex',
        flexFlow:'column',
        marginRight:'5px',
        '&>h3':{
            lineHeight:'0.5em',
            marginTop:'10px'
        }
    }
};

class RegOneTwo extends React.Component{
    constructor(props){
        super(props);
        this.classes = this.props.classes;

        this.state= {
            guestNumber:this.props.numGuests,
            guests: []
        }

        this. returnGuests = this.returnGuests.bind(this);
    }



    returnGuests = () =>{
        if(this.guestNumber === 0){return}
        var guests = [];
        var classes = this.classes;
        for (var i = 0; i < this.state.guestNumber; i++){
            guests.push(
            <div key={i}>
            <Typography className={classes.question} variant="body2">Guest {i+1}</Typography>
            <div className={classes.regInfo}>
                    <div className={classes.regName}>
                        <div className={classes.nameSec}>
                            <Typography variant="subheading">Random Guest</Typography>
                            <Typography variant="caption">name</Typography>
                        </div>
                    </div>
                    <div className={classes.regName}>
                        <div className={classes.nameSec}>
                            <Typography variant="subheading">ryansemail@gmail.com</Typography>
                            <Typography variant="caption">email</Typography>
                        </div>
                    </div>
                    <div className={classes.regName}>
                        <div className={classes.nameSec}>
                            <Typography variant="subheading">23145131</Typography>
                            <Typography variant="caption">phone</Typography>
                        </div>
                    </div>
                </div>
                </div>)
        
        }
        return guests;
    }

    render(){
        var classes = this.classes;
        return(
            <div className={classes.rootContainer}>
            <div className={classes.regOneRoot}>
                <Typography variant="title">Confirm your information</Typography>
                <Typography className={classes.question} variant="body2">You</Typography>
                <div className={classes.regInfo}>
                    <div className={classes.regName}>
                        <div className={classes.nameSec}>
                            <Typography variant="subheading">{this.props.registrant.firstName + " " + this.props.registrant.lastName}</Typography>
                            <Typography variant="caption">name</Typography>
                        </div>
                    </div>
                    <div className={classes.regName}>
                        <div className={classes.nameSec}>
                            <Typography variant="subheading">{this.props.registrant.email !== '' ? this.props.registrant.email : 'N/A'}</Typography>
                            <Typography variant="caption">email</Typography>
                        </div>
                    </div>
                    <div className={classes.regName}>
                        <div className={classes.nameSec}>
                            <Typography variant="subheading">{this.props.registrant.phone !== '' ? this.props.registrant.phone : 'N/A'}</Typography>
                            <Typography variant="caption">phone</Typography>
                        </div>
                    </div>
                </div>
                {this.returnGuests()}
            </div>
            </div>
        )
    }
}

export default withTheme()(withStyles(styles)(RegOneTwo));