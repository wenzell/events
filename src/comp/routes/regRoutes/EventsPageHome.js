import React from 'react';
import classnames from 'classnames'; 
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import Grow from '@material-ui/core/Zoom';
import Chip from '@material-ui/core/Chip';
import EmailIcon from '@material-ui/icons/Email';
import PhoneIcon from '@material-ui/icons/Phone';
import PlaceIcon from '@material-ui/icons/Place';
import SvgIcon from '@material-ui/core/SvgIcon';
import ChapelIcon from '../../../icons/chapel.svg';
import { withTheme, withStyles } from '@material-ui/core/styles';

const styles= {
    chips : {
        height:'60px',
        alignItems:'center',
        display: 'flex',
        flexFlow:'row wrap',
        padding:'10px 24px',
    },
    chipRoot:{
        marginRight:'20px',
        marginTop:'20px',
        cursor:'pointer',
        '&:hover':{
            backgroundColor:'#eee'
        },
        '&:active':{
            transition:'0s',
            backgroundColor:'#aaa'
        }
    },
    chipAvatar : {
        backgroundColor: '#00bcd4',
        color: '#fff',
        fontWeight:500,
    },
    eventInfoContainer:{
        padding:'20px 0',
        display:'flex',
        '@media screen and (max-width:768px)':{
            flexFlow:'column',
        }
    },
    eventInfoContainerLeft:{
        width:'40%',
        '@media screen and (max-width:768px)':{
            width:'100%'
        }
    },
    eventInfoContainerRight:{
        width:'50%',
        '@media screen and (max-width:768px)':{
            width:'100%'
        }
    },
    email:{
        display:'flex',
        alignItems: 'center',
        marginLeft:'20px',
        marginBottom:'2px',
        '&>p':{
            marginLeft:'5px'
        }
    },
    address:{
        transition:'0.3s',
        cursor:'pointer',
        '&:hover':{
            '& > svg':{
                transform: 'translate3D(0,-0.35em,0)'
            }
        },
        '&>svg':{
            transition:'0.3s',
        }
    },
    desc:{
        marginTop:'20px'
    },
    localeGroup:{
        display:'flex',
        alignItems:'center',
        margin:'10px 25px',
        cursor:'pointer',
        '&>img':{
            width:'35px',
            height:'35px'
        },
        '&:hover':{
            '&>.z':{
                backgroundColor:'rgba(139,194,73,0.2)'
            }
        }
    },
    localeName:{
        marginLeft:'-20px',
        transition:'0.15s',
        zIndex:'0',
        padding:'0 10px 0 30px',
        borderRadius:'5px',
        backgroundColor:'rgba(139,194,73,0.3)',
        height:'35px',
        display:'flex',
        flexFlow:'column',
        justifyContent:'center',
        userSelect:'none'
    },
    
};

class EventsPageHome extends React.Component{
    constructor(props){
        super(props);
        this.classes = this.props.classes;

        this.state= {
        }
    }

    render(){
        var classes = this.props.classes;
        return(
            <div>
                <Chip classes={{root:classes.chipRoot}} avatar={<Avatar classes={{root:classes.chipAvatar}}>D</Avatar>} label="Northwest California" />
                <Chip classes={{root:classes.chipRoot}} avatar={<Avatar classes={{root:classes.chipAvatar}}>L</Avatar>} label="San Francisco" />
                <div className={classes.eventInfoContainer}>
                    <div className={classes.eventInfoContainerLeft}>
                        <Typography variant="body2">Contact</Typography>
                        <div className={classes.email}>
                            <EmailIcon color="primary" />
                            <Typography variant="body1">nwc_cfo@gmail.com</Typography>
                        </div>
                        <div className={classes.email}>
                            <PhoneIcon color="primary" />
                            <Typography variant="body1">925-395-2349</Typography>
                        </div>
                        <Typography variant="body2">Location</Typography>
                        <div className={classnames(classes.email,classes.address)}>
                            <PlaceIcon color="primary" />
                            <Typography variant="body1">2433 Street Court<br/>San Francisco, CA 94123</Typography>
                        </div>
                    </div>
                    <div className={classes.eventInfoContainerRight}>
                        <Typography variant="body2">Description</Typography>
                        <div className={classes.email}>
                            <Typography variant="body1">Join the Local Congregation of San Francisco for their bi-monthly cooking night. This time it's going to be sushi! Bring your friends and family.</Typography>
                        </div>
                        <Typography className={classes.desc} variant="body2">Learn more about this Locale Congregation</Typography>
                        <div className={classes.localeGroup}>
                            <img src={ChapelIcon}/>
                            <div className={classes.localeName + ' z'}>
                                <Typography variant='body1'>San Francisco</Typography>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withTheme()(withStyles(styles)(EventsPageHome));