import React from 'react';
import Grow from '@material-ui/core/Zoom';
import FBIcon from 'mdi-material-ui/Facebook';
import TwitterIcon from 'mdi-material-ui/Twitter';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { withTheme, withStyles } from '@material-ui/core/styles';

const styles= {
    eventsShareRoot:{
        display:'flex',
        flexFlow:'column',
        alignItems:'center'
    },
    pbody:{
        marginTop:'20px',
        lineHeight:'1.3em',
        width:'50%',
        minWidth:'250px',
        textAlign:'justify'
    }
};

class EventsShare extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            
        }
    }

    render(){
        var classes = this.props.classes;
        return(
            <div className={classes.eventsShareRoot}>
                <Typography className={classes.pbody} variant="body1">Thank you for registering!<br/><br/>You'll receive an email notification and reminders about the event. In the meantime, why not tell everyone you're going?<br/><br/>Click below to share!</Typography>
                <div className={styles.socialIcons}>
                    <Grow in={true} style={{transitionDelay: 50 }}>
                        <IconButton>
                            <FBIcon style={{color:'#3B5998'}} />
                        </IconButton>
                    </Grow>
                    <Grow in={true} style={{transitionDelay: 150 }}>
                        <IconButton>
                            <TwitterIcon style={{color:'#00aced'}} />
                        </IconButton>
                    </Grow>
                </div>
            </div>
        )
    }
}

export default withTheme()(withStyles(styles)(EventsShare));