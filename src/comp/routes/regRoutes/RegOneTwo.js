import React from 'react';
import { withTheme, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import Button from '@material-ui/core/Button';




const styles= {
    regOneRoot:{
        width:'100%',
        display:'flex',
        flexFlow:'column',
        padding:'20px',
    },
    textField:{
        marginRight:'30px'
    },
    noWrap:{
        flexWrap:'nowrap'
    },
    question:{
        marginTop:'50px'
    },
    numberPicker:{
      display:'flex',
      alignItems:'center',
      marginTop:'20px',
    },
    numButton:{
        fontWeight:'300',
        fontSize:'1.5em',
        color:'#444',
        minWidth:'30px',
        minHeight:'30px',
        padding:0,
        lineHeight:0
    },
    guestNumber:{
        padding:'0 20px',
        backgroundColor:'#f3f3f3',
        height:'30px',
        display:'flex',
        flexFlow: 'column',
        justifyContent:'center',
        margin:'0 -1px',
        boxShadow:' 0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);'
    }
};

class RegOneTwo extends React.Component{
    constructor(props){
        super(props);
        this.classes = this.props.classes;

        this.state= {
            guestNumber:this.props.numGuests,
            guests: []
        }

        this.returnGuests = this.returnGuests.bind(this);
    }




    returnGuests = () =>{
        var guests = [];
        var classes = this.classes;
        for (var i = 0; i < this.props.numGuests; i++){
            guests.push(
            <div key={i}><Typography className={classes.question} variant="body2">Guest {i+1}</Typography>
            <FormControl>
                <FormGroup row>
                    <TextField className={classes.textField} label="First Name"/>
                    <TextField className={classes.textField} label="Last Name"/>
                </FormGroup>
                <FormGroup row>
                    <TextField className={classes.textField} label="email"/>
                    <TextField inputProps={{type:'tel'}} className={classes.textField} label="phone"/>
                </FormGroup>
            </FormControl></div>)
        
        }
        return guests;
    }

    render(){
        var classes = this.classes;
        return(
            <div className={classes.regOneRoot}>
                <FormControl >
                    <Typography  variant="body2">Are you a...</Typography>
                    <RadioGroup value={this.props.registrant.type} onChange={this.props.handleRadio}>
                        <FormControlLabel value="member" control={<Radio />} label="Member" />
                        <FormControlLabel value="biblestudent" control={<Radio />} label="Bible Student" />
                        <FormControlLabel value="guest" control={<Radio />} label="Guest" />
                    </RadioGroup>
                </FormControl>
                <FormControl>
                    <Typography className={classes.question} variant="body2">Tell us about yourself</Typography>
                    <FormGroup row>
                        <TextField className={classes.textField} required value={this.props.registrant.firstName} onChange={this.props.handleRegistrant('firstName')} label="First Name"/>
                        <TextField className={classes.textField} required value={this.props.registrant.lastName} onChange={this.props.handleRegistrant('lastName')} label="Last Name"/>
                    </FormGroup>
                    <FormGroup row>
                        <TextField className={classes.textField} value={this.props.registrant.email} onChange={this.props.handleRegistrant('email')} label="email"/>
                        <TextField inputProps={{type:'tel'}} className={classes.textField} value={this.props.registrant.phone} onChange={this.props.handleRegistrant('phone')}  label="phone"/>
                    </FormGroup>
                </FormControl>
                <Typography className={classes.question} variant="body2">How many guests are you bringing?</Typography>
                <div className={classes.numberPicker}>
                    <Button className={classes.numButton} onClick={()=>{this.props.changeGuestNumber(0)}} variant="raised" size="small">-</Button>
                    <Typography className={classes.guestNumber}>{this.props.numGuests}</Typography>
                    <Button className={classes.numButton} onClick={()=>{this.props.changeGuestNumber(1)}} variant="raised" size="small">+</Button>
                </div>
                {this.returnGuests()}
            </div>
        )
    }
}

export default withTheme()(withStyles(styles)(RegOneTwo));