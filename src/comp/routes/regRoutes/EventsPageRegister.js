import React from 'react';
import { Switch, Route, Redirect} from 'react-router-dom';
import { withTheme, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import RegOneTwo from '../regRoutes/RegOneTwo';
import RegThree from '../regRoutes/RegThree';
import EventsShare from '../regRoutes/EventsShare';

const styles = (theme) => {
    return({
        register:{
            display:'flex',
            flexFlow:'column',
            width:'100%',
        },
        regTitle:{
            backgroundColor:theme.palette.primary.main,
            margin:'-20px',
            padding:'10px 20px',
            marginBottom:'40px',
            '&>h1':{
                color:'#fff',
                fontWeight:'300',
                '@media screen and (max-width:768px)':{
                    fontSize:'2em'
                }
            }
        }
    })
};

class EventsPageRegister extends React.Component{
    constructor(props){
        super(props);
        this.classes = this.props.classes;
        this.state = {
            numGuests : 0,
            guests : [],
            registrantType:'',
            registrant:{
                firstName:'',lastName:'',email:'',phone:'',type:''
            }
        }
        this.getRegState= this.getRegState.bind(this);
        this.handleRadio = this.handleRadio.bind(this);
        this.changeGuestNumber = this.changeGuestNumber.bind(this);
        this.handleRegistrant = this.handleRegistrant.bind(this);
    }

    handleRadio = (event) =>{
        var newRegistrant = this.state.registrant;
        newRegistrant.type = event.target.value;
        this.setState({registrant : newRegistrant})
    }

    handleRegistrant = name => event =>{
        var newRegistrant = this.state.registrant;
        newRegistrant[name]= event.target.value;
        this.setState({
            registrant: newRegistrant
        })
    }

    changeGuestNumber = (op) =>{
        if(op === 1){
            this.setState({
                numGuests: this.state.numGuests + 1
            })
        }
        else{
            if(this.state.numGuests !==0){
                this.setState({
                    numGuests: this.state.numGuests - 1,
                })
            }   
        }
    }


    getRegState = () =>{
        switch(this.props.regState){
            case 1:
                return <RegOneTwo changeGuestNumber={this.changeGuestNumber} handleRadio={this.handleRadio} registrant={this.state.registrant} handleRegistrant={this.handleRegistrant} numGuests={this.state.numGuests} />;
                break;
            case 2:
                return <RegThree registrant={this.state.registrant} numGuests={this.state.numGuests} />;
                break;
            case 3:
                return <EventsShare />
            default:
                return <Redirect push to={this.props.ogUrl} />
        }
    }
    

    render(){
        var classes = this.props.classes;
        return(
            <div className={classes.register}>
                <div className={classes.regTitle}>
                    <Typography variant="display3">REGISTRATION</Typography>
                </div>
                {this.getRegState()}
            </div>
        )
    }
}

export default withTheme()(withStyles(styles)(EventsPageRegister));