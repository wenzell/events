import React from 'react';
import Grow from '@material-ui/core/Grow';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ViewWeekIcon from '@material-ui/icons/ViewColumn';
import { withTheme, withStyles } from '@material-ui/core/styles';


import EventCard from '../subcomp/EventCard';

const styles= {
    root:{
        flexGrow: 1,
        display:'flex',
        flexFlow: 'column',
        padding:'30px'
    },
    fontthree:{
        fontWeight: '300',
        marginBottom:'20px'
    },
    grid:{
        alignSelf:'center',
        justifyContent:'center',
        width:'100%',
        '&.row':{
            justifyContent:'initial',
            flexFlow:'row',
            overflow:'auto',
        },
        '@media screen and (max-width: 768px)':{
            justifyContent:'center',
            '&.row':{
                justifyContent:'initial',
                paddingRight:0
            }
        }
    },
    head :{
        display: 'flex',
        justifyContent: 'space-between',
    }
};

class EventsHome extends React.Component{
    constructor(props){
        super(props);
        this.classes = this.props.classes;

        this.state= {
            isModules: true,
            growEnable:true,
            events: [1,2,3,4,5,6,7,4]
        }

    }
    

    changeView = () =>{
        this.setState({
            isModules: !this.state.isModules,
            growEnable: false
        })
        window.setTimeout(()=>{this.setState({
            growEnable:true
        })},10)
        
    }

    render(){
        return(
            <div className={this.classes.root}>
                <div className={this.classes.head}>
                    <Typography className={this.classes.fontthree} variant="display2">My Events</Typography>
                    <IconButton onClick={()=>{this.changeView()}}>
                        {this.state.isModules ? <ViewModuleIcon/> : <ViewWeekIcon/>}
                    </IconButton>
                </div>
                <Grid style={{display:this.state.growEnable? 'flex': 'none'}} className={this.classes.grid + " " + (this.state.isModules ? null : 'row' )} container spacing={24}>
                    {this.state.events.map((o,i)=>{
                        return (
                            <Grow key={i} in={this.state.growEnable} style={{transitionDelay: this.state.growEnable ? 100 + i*50 : 0 }}>
                                <Grid item>
                                    <EventCard />
                                </Grid>
                            </Grow>
                        )
                    })}
                </Grid>
            </div>
        )
    }
}

export default withTheme()(withStyles(styles)(EventsHome));