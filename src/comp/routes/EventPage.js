import React from 'react';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import Grow from '@material-ui/core/Zoom';
import FBIcon from 'mdi-material-ui/Facebook';
import TwitterIcon from 'mdi-material-ui/Twitter';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import { Switch, Route, Link, withRouter } from 'react-router-dom';
import { withTheme, withStyles } from '@material-ui/core/styles';
import $ from 'jquery';


//EventPageRoutes
import EventPageHome from '../routes/regRoutes/EventsPageHome';
import EventsPageRegister from '../routes/regRoutes/EventsPageRegister';

const styles= {
    eventPageRoot:{
        display:'flex',
        flexFlow:'column',
        position:'relative',
        alignItems:'center',
    },
    eventImage:{
        display: 'block',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        position: 'absolute',
        top: '0px',
        left:'0',
        right:'0',
        height: '380px',
        zIndex: '-1',
        '@media screen and (max-width: 768px)':{
            height:'211px'
        }
    },
    rootPaper:{
        margin:'250px 5%',
        width:'90%',
        maxWidth:'850px',
        padding:'20px',
        '@media screen and (max-width: 768px)':{
            margin:'180px 5% 100px 5%',
        }
    },
    divider:{
        margin: '20px -20px',
    },
    rootTitle: {
        fontWeight:'500',
        color:'#4a4a4a',
        '@media screen and (max-width: 768px)':{
            fontSize:'2em'
        }
    },
    rootSub:{
        fontSize:'1.5em',
        '@media screen and (max-width: 768px)':{
            fontSize:'1em'
        }
    },
    cardActions:{
        display:'flex',
        alignItems:'center'
    },
    socialIcons:{
    },
    registerButton:{
        marginLeft:'auto',
        textDecoration:'none'
    }
};


const SocialIcons = ({ match }) => {
    return(
        <div className={styles.socialIcons}>
            <Grow in={true} style={{transitionDelay: 50 }}>
                <IconButton>
                    <FBIcon style={{color:'#3B5998'}} />
                </IconButton>
            </Grow>
            <Grow in={true} style={{transitionDelay: 150 }}>
                <IconButton>
                    <TwitterIcon style={{color:'#00aced'}} />
                </IconButton>
            </Grow>
        </div>
    )
}


const scrollTop = () =>{
    $('html,body').animate({ "scrollTop": 0 }, 400);
}


class EventPage extends React.Component{
    constructor(props){
        super(props);


        //regState
        //0 = event info
        //1 = event registration
        //2 = registration confirmation
        //3 = share

        this.state= {
            prev:"",
            regState:0,
            regText: "Register"
        }
        this.handleRegisterButton = this.handleRegisterButton.bind(this);
        this.handleBackButton = this.handleBackButton.bind(this);
        this.returnButton = this.returnButton.bind(this);
    }

    handleBackButton = () =>{
        switch(this.state.regState){
            case 1:
                this.setState({regState:0});
                break;
            case 2:
                this.setState({regState:1});
                break;
            case 3:
                this.setState({regState:0});
                break;
            default:
                this.setState({regState:0});
        }
    }

    BackButton = ({ match }) => {
        return(
            <Button onClick={()=>{this.handleBackButton()}} size="large">BACK</Button>
        )
    }
    
    handleRegisterButton = () =>{
        switch(this.state.regState){
            case 0:
                this.setState({regState:1});
                scrollTop();
                break;
            case 1:
                this.setState({regState:2});
                scrollTop();
                break;
            case 2:
                this.setState({regState:3});
                scrollTop();
                break;
        }
    }


    returnPage = () => {
        switch(this.state.regState){
            case 0:
                return <EventPageHome />;
                break;
            case 1:
                return <EventsPageRegister regState={1} />;
                break;
            case 2:
                return <EventsPageRegister regState={2} />;
                break;
            case 3:
                return <EventsPageRegister regState={3} />;
                break;
            default:
                return <EventPageHome />;
        }
    }

    returnButton = () =>{
        var BackButton = this.BackButton;
        switch(this.state.regState){
            case 0:
                return <SocialIcons />;
                break;
            default:
                return <BackButton />;
        }
    }
    

    render(){
        var classes = this.props.classes
        return(
            <div className={classes.eventPageRoot}>
                <div className={classes.eventImage} style={{backgroundImage: 'url(http://via.placeholder.com/1280x720)'}}> 
                </div>
                <Paper className={classes.rootPaper} elevation={3}>
                    <Typography className={classes.rootTitle} variant="display3">Event Name</Typography>
                    <Typography className={classes.rootSub} variant="headline" >9PM Saturday, September 4, 2018</Typography>
                    <Divider className={classes.divider} />
                    {this.returnPage()}
                    <Divider className={classes.divider} />
                    
                    <div className={classes.cardActions}>
                    {this.returnButton()}
                    {this.state.regState !== 3 ? (<Grow in={true} style={{transitionDelay: 250}}>
                            <Button onClick={()=>{this.handleRegisterButton()}} className={classes.registerButton} size="large"  variant="raised" color="primary">{this.state.regState !== 2 ? 'REGISTER' : 'CONFIRM'}</Button>
                        </Grow>) : null}
                        
                    </div>
                </Paper>
            </div>
        )
    }
}

export default withRouter(withTheme()(withStyles(styles)(EventPage)));