import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import './App.css';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import EventsAppBar from './comp/EventsAppBar';
import EventsDrawer from './comp/EventsDrawer';
import EventsBrowser from './comp/EventsBrowser';


const theme = createMuiTheme({
	palette: {
		primary: {
			light: "#62efff",
			main : "#00bcd4",
			dark : "#008ba3",
			contrastText: '#fff',
		},
		secondary: {
			light: "#718792",
			main : "#455a64",
			dark: "1c313a",
			contrastText: '#fff'
		},
		values: {
			md:768,
		},
		text: {
			primary: "#444444",
		}
	}
});


class App extends React.Component {
	constructor(props){
		super(props);

		this.state={
			drawerOpen : false
		}
	}

	handleDrawer = () =>{
		var tState = !this.state.drawerOpen;
		this.setState({
			drawerOpen : tState
		})
	}

	render() {
		return (
			<MuiThemeProvider theme={theme}>
				<div className="app">
					<CssBaseline />
					<EventsAppBar handleDrawer={this.handleDrawer} />
					<EventsDrawer drawerOpen={this.state.drawerOpen} />
					<EventsBrowser drawerOpen={this.state.drawerOpen} />
				</div>
			</MuiThemeProvider>
		);
	}
}

export default App;
