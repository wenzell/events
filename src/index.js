import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Router } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import $ from 'jquery';

const browserHistory = createHistory();

browserHistory.listen((location,action)=>{
    $('html,body').animate({ "scrollTop": 0 }, 400);
})


ReactDOM.render(<Router history={browserHistory} ><App /></Router>, document.getElementById('root'));
registerServiceWorker();
